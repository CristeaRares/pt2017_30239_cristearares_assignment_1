import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class GUI {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	Polinom p = new Polinom();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPolinom = new JLabel("Polinom 1:");
		lblPolinom.setBounds(10, 11, 78, 14);
		frame.getContentPane().add(lblPolinom);
		
		JLabel lblPolinom_1 = new JLabel("Polinom 2:");
		lblPolinom_1.setBounds(10, 36, 78, 14);
		frame.getContentPane().add(lblPolinom_1);
		
		JLabel lblRezultat = new JLabel("Rezultat");
		lblRezultat.setBounds(10, 61, 78, 14);
		frame.getContentPane().add(lblRezultat);
		
		textField = new JTextField();
		textField.setBounds(122, 8, 238, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(122, 33, 238, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(122, 58, 238, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				List<Monom> polinomyalList = new ArrayList();
				List<Monom> polinomyalList1 = new ArrayList();
				
				String pol1 = textField.getText();
				String[] pol1Values = pol1.split(",");
				String pol2 = textField_1.getText();
				String[] pol2Values = pol2.split(",");
				
				for (int i = 0; i < pol1Values.length; i += 2) {
					polinomyalList.add(new Monom(Double.parseDouble(pol1Values[i]), Integer.parseInt(pol1Values[i + 1])));
				}
				p.polinomyalList = polinomyalList;
				for (int i = 0; i < pol2Values.length; i += 2) {
					polinomyalList1.add(new Monom(Double.parseDouble(pol2Values[i]), Integer.parseInt(pol2Values[i + 1])));
				}
				p.polinomyalList1 = polinomyalList1;

			}
		});
		btnStart.setBounds(175, 90, 89, 23);
		frame.getContentPane().add(btnStart);
		
		JButton btnAdunare = new JButton("Adunare");
		btnAdunare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				p.addPolinom();
				textField_2.setText(p.displayAdunare());
				
			}
		});
		btnAdunare.setBounds(10, 138, 102, 23);
		frame.getContentPane().add(btnAdunare);
		
		JButton btnScadere = new JButton("Scadere");
		btnScadere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.scaderePolinom();
				textField_2.setText(p.displayScadere());
				
			}
		});
		btnScadere.setBounds(322, 138, 102, 23);
		frame.getContentPane().add(btnScadere);
		
		JButton btnInmultire = new JButton("Inmultire");
		btnInmultire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.inmultirePolinom();
				textField_2.setText(p.displayInmultire());
			}
		});
		btnInmultire.setBounds(10, 172, 102, 23);
		frame.getContentPane().add(btnInmultire);
		
		JButton btnImpartire = new JButton("Impartire");
		btnImpartire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.impartirePolinom();
				textField_2.setText(p.displayImpartire()+" rest "+p.displayImpartireRest());
			}
		});
		btnImpartire.setBounds(322, 172, 102, 23);
		frame.getContentPane().add(btnImpartire);
		
		JButton btnDerivarep = new JButton("DerivareP1");
		btnDerivarep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.derivarePolinom();
				textField_2.setText(p.displayDerivare());
				
			}
		});
		btnDerivarep.setBounds(10, 206, 102, 23);
		frame.getContentPane().add(btnDerivarep);
		
		JButton btnIntegrarep = new JButton("IntegrareP1");
		btnIntegrarep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.integrarePolinom();
				textField_2.setText(p.displayIntegrare());
			}
		});
		btnIntegrarep.setBounds(322, 206, 102, 23);
		frame.getContentPane().add(btnIntegrarep);
		
		
	}
}
