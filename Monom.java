import java.util.Scanner;

public class Monom {
	private double coef;
	private int grad;
	private boolean isSeen;
	Scanner x = new Scanner(System.in);

	public Monom(double coeficient, int gradul) {
		coef = coeficient;
		grad = gradul;
	}

	public Monom() {

	}

	public void setSeen(boolean isSeen) {
		this.isSeen = isSeen;
	}

	public boolean isSeen() {
		return isSeen;
	}

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	public double readCoef() {
		coef = x.nextDouble();
		return coef;
	}

	public int readGrad() {
		grad = x.nextInt();
		return grad;
	}

	public String toString() {
		return this.getCoef() + "x^" + this.getGrad();
	}
}
