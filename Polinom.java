import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
public class Polinom{
  

  List<Monom> polinomyalList=new ArrayList();
  List<Monom> polinomyalList1=new ArrayList();
  List<Monom> polinomyalList2=new ArrayList();
  List<Monom> polinomyalList3=new ArrayList();
  List<Monom> polinomyalList4=new ArrayList();
  
  Scanner x=new Scanner(System.in);
//  public  void readPolinom1(){
//	  int i=0;
//	  while(true){
//		  System.out.println("Insert the coefficient nr:"+(i+1));
//		  final double coefficient=x.nextDouble();
//		  if(coefficient==0){
//			  break;
//		  }
//		  System.out.println("Insert the degree nr:"+(i+1));
//		  final int polyGrad=x.nextInt();
//		  if(polyGrad<0){
//			  break;
//		  }
//		  final Monom dummy=new Monom(coefficient,polyGrad);
//		  polinomyalList.add(dummy);
//		  i++;
//	  } 
//	  }
//	  public void readPolinom2(){
//	  int i=0;
//	  while(true){
//		  System.out.println("Insert the coefficient nr"+(i+1));
//		  final double coefficient1=x.nextDouble();
//		  if(coefficient1==0){
//			  break;
//		  }
//		  System.out.println("insert the degree nr"+(i+1));
//		  final int polyGrad1=x.nextInt();
//		  if(polyGrad1<0){
//			  break;
//		  }
//		  final Monom dummy=new Monom(coefficient1,polyGrad1);
//		  polinomyalList1.add(dummy);
//		  i++;
//	  }
//  }
	  public void addPolinom(){
		int i,j,k;
	    double coefficient;
	    int grad;
	    
	    
		for (i=0;i<polinomyalList.size();i++){
			for(j=0;j<polinomyalList1.size();j++){
				 
				if(polinomyalList.get(i).getGrad()==polinomyalList1.get(j).getGrad()&& !polinomyalList1.get(j).isSeen() &&!polinomyalList.get(i).isSeen()){
					coefficient=polinomyalList.get(i).getCoef()+polinomyalList1.get(j).getCoef();
					grad=polinomyalList.get(i).getGrad();
					final Monom dummy=new Monom(coefficient,grad);
					polinomyalList2.add(dummy);
					polinomyalList.get(i).setSeen(true);
					polinomyalList1.get(j).setSeen(true);
				}
			}	
		}
		for (i=0;i<polinomyalList.size();i++){
			if(polinomyalList.get(i).isSeen()==false){
		polinomyalList2.add(polinomyalList.get(i));
			}
		}
		for (i=0;i<polinomyalList1.size();i++){
			if(polinomyalList1.get(i).isSeen()==false){
		polinomyalList2.add(polinomyalList1.get(i));
			}
		}
		for(i=0;i<polinomyalList2.size()-1;i++){
			for(j=i+1;j<polinomyalList2.size();j++){
				if(polinomyalList2.get(i).getGrad()<polinomyalList2.get(j).getGrad()){

					Collections.swap(polinomyalList2, i, j);
					
				}
			}
		}
	  }
	public String displayAdunare(){
		String result = "";
		int i;
		System.out.println("Adunare:");
		for (i=0;i<polinomyalList2.size();i++){
			
			if(i<polinomyalList2.size()){
				if(polinomyalList2.get(i).getCoef()<0){
					result=result + "";
					System.out.print("");
				}else if(polinomyalList2.get(i).getCoef()!=0){
					result = result + "+";
				System.out.print("+");
				}
			}
			if (polinomyalList2.get(i).getCoef()!=0){
			result = result + polinomyalList2.get(i).toString(); 
			System.out.print(polinomyalList2.get(i).toString());
			}
		}
		result=result.replace("x^0", "");
		return result;
	}
	  public void scaderePolinom(){
			int i,j;
		    double coefficient;
		    int grad;
		    
		    
			for (i=0;i<polinomyalList.size();i++){
				for(j=0;j<polinomyalList1.size();j++){
					 
					if(polinomyalList.get(i).getGrad()==polinomyalList1.get(j).getGrad()&& !polinomyalList1.get(j).isSeen() &&!polinomyalList.get(i).isSeen()){
						coefficient=polinomyalList.get(i).getCoef()-polinomyalList1.get(j).getCoef();
						grad=polinomyalList.get(i).getGrad();
						final Monom dummy=new Monom(coefficient,grad);
						polinomyalList2.add(dummy);
						polinomyalList.get(i).setSeen(true);
						polinomyalList1.get(j).setSeen(true);
					}
				}					
			}
			for (i=0;i<polinomyalList.size();i++){
				if(polinomyalList.get(i).isSeen()==false){
			polinomyalList2.add(polinomyalList.get(i));
				}
			}
			for (i=0;i<polinomyalList1.size();i++){
				if(polinomyalList1.get(i).isSeen()==false){
					coefficient=-polinomyalList1.get(i).getCoef();
					grad=polinomyalList1.get(i).getGrad();
					Monom dummy=new Monom(coefficient,grad);
			polinomyalList2.add(dummy);
				}
			}
			for(i=0;i<polinomyalList2.size()-1;i++){
				for(j=i+1;j<polinomyalList2.size();j++){
					if(polinomyalList2.get(i).getGrad()<polinomyalList2.get(j).getGrad()){

						Collections.swap(polinomyalList2, i, j);
						
					}
				}
			}
		  }
	  public String displayScadere(){
			String result = "";
			int i;
			System.out.println("Scadere:");
			for (i=0;i<polinomyalList2.size();i++){
				
				if(i<polinomyalList2.size()){
					if(polinomyalList2.get(i).getCoef()<0){
						result=result + "";
						System.out.print("");
					}else if(polinomyalList2.get(i).getCoef()!=0){
						result = result + "+";
					System.out.print("+");
					}
				}
				if (polinomyalList2.get(i).getCoef()!=0){
				result = result + polinomyalList2.get(i).toString(); 
				System.out.print(polinomyalList2.get(i).toString());
				}
			}
			result=result.replace("x^0", "");
			return result;
		}
		public void inmultirePolinom(){
			int i,j,nr;
			double coefficient;
			int grad;
			
			for(i=0;i<polinomyalList.size();i++){				
				for(j=0;j<polinomyalList1.size();j++){
					coefficient=polinomyalList.get(i).getCoef()*polinomyalList1.get(j).getCoef();
					grad=polinomyalList.get(i).getGrad()+polinomyalList1.get(j).getGrad();
					final Monom dummy=new Monom(coefficient,grad);
					polinomyalList2.add(dummy);
				}
			}
			
            for(i=0;i<polinomyalList2.size()-1;i++){
            	coefficient=polinomyalList2.get(i).getCoef();
            	grad=polinomyalList2.get(i).getGrad();
            	nr=0;
            	for (j=i+1;j<polinomyalList2.size();j++){
            		
            		if(polinomyalList2.get(i).getGrad()==polinomyalList2.get(j).getGrad() && !polinomyalList2.get(j).isSeen()){

            			nr+=1;
            			coefficient+=polinomyalList2.get(j).getCoef();
            			polinomyalList2.get(i).setSeen(true);
            			polinomyalList2.get(j).setSeen(true);            			
                        	}
            	}
            	if(polinomyalList2.get(i).isSeen()==true && nr>=1){
            	final Monom dummy=new Monom(coefficient,grad);
    			polinomyalList3.add(dummy);
            	}
            }
			
            for(i=0;i<polinomyalList2.size();i++){
            	if(polinomyalList2.get(i).isSeen()==false){
            		polinomyalList3.add(polinomyalList2.get(i));
            	}
            }
			
			for(i=0;i<polinomyalList3.size()-1;i++){
				for(j=i+1;j<polinomyalList3.size();j++){
					if(polinomyalList3.get(i).getGrad()<polinomyalList3.get(j).getGrad()){

						Collections.swap(polinomyalList3, i, j);
						
					}
				}
			}
		}
		public String displayInmultire(){
			String result = "";
			int i;
			System.out.println("Inmultire:");
			for (i=0;i<polinomyalList3.size();i++){
				
				if(i<polinomyalList3.size()){
					if(polinomyalList3.get(i).getCoef()<0){
						result=result + "";
						System.out.print("");
					}else if(polinomyalList3.get(i).getCoef()!=0){
						result = result + "+";
					System.out.print("+");
					}
				}
				if (polinomyalList3.get(i).getCoef()!=0){
				result = result + polinomyalList3.get(i).toString(); 
				System.out.print(polinomyalList3.get(i).toString());
				}
			}
			result=result.replace("x^0", "");
			return result;
		}
		
		public void impartirePolinom(){
			int i,j,nr=0,k=0;
			 double coefficient;
			 int grad;
			 for(i=0;i<polinomyalList.size()-1;i++){
					for(j=i+1;j<polinomyalList.size();j++){
						if(polinomyalList.get(i).getGrad()<polinomyalList.get(j).getGrad()){

							Collections.swap(polinomyalList, i, j);
							
						}
					}
				}
			 for(i=0;i<polinomyalList1.size()-1;i++){
					for(j=i+1;j<polinomyalList1.size();j++){
						if(polinomyalList1.get(i).getGrad()<polinomyalList1.get(j).getGrad()){

							Collections.swap(polinomyalList1, i, j);
							
						}
					}
				}
			 
			
			 while(Math.abs(polinomyalList.get(nr).getCoef())>=Math.abs(polinomyalList1.get(0).getCoef()) && polinomyalList.get(nr).getGrad()>=polinomyalList1.get(0).getGrad()){
			 coefficient=polinomyalList.get(nr).getCoef()/polinomyalList1.get(0).getCoef();
			 grad=polinomyalList.get(nr).getGrad()-polinomyalList1.get(0).getGrad();
			 if(coefficient<0){
				 coefficient=Math.ceil(coefficient);
			 }else{
				 coefficient=Math.floor(coefficient);
			 }
			 Monom dummy=new Monom(coefficient,grad);
			 polinomyalList2.add(dummy);
			 
			 
            for(i=0;i<polinomyalList1.size();i++){
            	coefficient=-polinomyalList1.get(i).getCoef()*polinomyalList2.get(nr).getCoef();
            	grad=polinomyalList1.get(i).getGrad()+polinomyalList2.get(nr).getGrad();
            	Monom aux=new Monom(coefficient,grad);
            	polinomyalList3.add(aux);
            }
           nr++;
           k++;
            //Adunare
            for (i=0;i<polinomyalList.size();i++){
    			for(j=0;j<polinomyalList3.size();j++){
    				if(polinomyalList.get(i).getGrad()==polinomyalList3.get(j).getGrad()&& !polinomyalList1.get(j).isSeen() &&!polinomyalList.get(i).isSeen()){
    					coefficient=polinomyalList.get(i).getCoef()+polinomyalList3.get(j).getCoef();
    					grad=polinomyalList.get(i).getGrad();
    					Monom dummy1=new Monom(coefficient,grad);
    					polinomyalList4.add(dummy1);
    					polinomyalList.get(i).setSeen(true);
    					polinomyalList3.get(j).setSeen(true);
    				}
    			}	
    		}
    		for (i=0;i<polinomyalList.size();i++){
    			if(polinomyalList.get(i).isSeen()==false){
    		polinomyalList4.add(polinomyalList.get(i));
    			}
    		}
    		for (i=0;i<polinomyalList3.size();i++){
    			if(polinomyalList3.get(i).isSeen()==false){
    		polinomyalList4.add(polinomyalList3.get(i));
    			}
    		}
    	
    		
    		///////////////////////////////////
    	
    		polinomyalList.clear();
    		polinomyalList.addAll(polinomyalList4);   		
    		polinomyalList3.clear();
    		polinomyalList4.clear();
    		
    		 for(i=0;i<polinomyalList.size()-1;i++){
					for(j=i+1;j<polinomyalList.size();j++){
						if(polinomyalList.get(i).getGrad()<polinomyalList.get(j).getGrad()){

							Collections.swap(polinomyalList, i, j);
							
						}
					}
				}
    		
		}  
    		 
		}
		
		 public String displayImpartire(){
				String result = "";
				int i;
				System.out.println("Impartire:");
				for (i=0;i<polinomyalList2.size();i++){
					
					if(i<polinomyalList2.size()){
						if(polinomyalList2.get(i).getCoef()<0){
							result=result + "";
							System.out.print("");
						}else if(polinomyalList2.get(i).getCoef()!=0){
							result = result + "+";
						System.out.print("+");
						}
					}
					if (polinomyalList2.get(i).getCoef()!=0){
					result = result + polinomyalList2.get(i).toString(); 
					System.out.print(polinomyalList2.get(i).toString());
					}
				}
				result=result.replace("x^0", "");
				return result;
			}
		 public String displayImpartireRest(){
				String result = "";
				int i;
				System.out.println();
				System.out.println("ImpartireRest:");
				for (i=0;i<polinomyalList.size();i++){
					
					if(i<polinomyalList.size()){
						if(polinomyalList.get(i).getCoef()<0){
							result=result + "";
							System.out.print("");
						}else if(polinomyalList.get(i).getCoef()!=0){
							result = result + "+";
						System.out.print("+");
						}
					}
					if (polinomyalList.get(i).getCoef()!=0){
					result = result + polinomyalList.get(i).toString(); 
					System.out.print(polinomyalList.get(i).toString());
					}
				}
				result=result.replace("x^0", "");
				return result;
			}
		public void derivarePolinom(){
			int i,j;
			double coefficient;
			int grad;
			for(i=0;i<polinomyalList.size();i++){
				coefficient=polinomyalList.get(i).getCoef()*(polinomyalList.get(i).getGrad());
				grad=polinomyalList.get(i).getGrad()-1;
				Monom dummy=new Monom(coefficient,grad);
				polinomyalList2.add(dummy);
			}
			for(i=0;i<polinomyalList2.size()-1;i++){
				for(j=i+1;j<polinomyalList2.size();j++){
					if(polinomyalList2.get(i).getGrad()<polinomyalList2.get(j).getGrad()){

						Collections.swap(polinomyalList2, i, j);
						
					}
				}
			}
		}
		public String displayDerivare(){
			String result = "";
			int i;
			System.out.println("Derivare:");
			for (i=0;i<polinomyalList2.size();i++){
				
				if(i<polinomyalList2.size()){
					if(polinomyalList2.get(i).getCoef()<0){
						result=result + "";
						System.out.print("");
					}else if(polinomyalList2.get(i).getCoef()!=0){
						result = result + "+";
					System.out.print("+");
					}
				}
				if (polinomyalList2.get(i).getCoef()!=0){
				result = result + polinomyalList2.get(i).toString(); 
				System.out.print(polinomyalList2.get(i).toString());
				}
			}
			result=result.replace("x^0", "");
			return result;
		}
		public void integrarePolinom(){
			int i,j;
			double coefficient;
			int grad;
			for(i=0;i<polinomyalList.size();i++){
				coefficient=polinomyalList.get(i).getCoef()/(polinomyalList.get(i).getGrad()+1);
				grad=polinomyalList.get(i).getGrad()+1;
				Monom dummy=new Monom(coefficient,grad);
				polinomyalList2.add(dummy);
			}
			for(i=0;i<polinomyalList2.size()-1;i++){
				for(j=i+1;j<polinomyalList2.size();j++){
					if(polinomyalList2.get(i).getGrad()<polinomyalList2.get(j).getGrad()){

						Collections.swap(polinomyalList2, i, j);
						
					}
				}
			}
		}
		public String displayIntegrare(){
			String result = "";
			int i;
			System.out.println("Integrare:");
			for (i=0;i<polinomyalList2.size();i++){
				
				if(i<polinomyalList2.size()){
					if(polinomyalList2.get(i).getCoef()<0){
						result=result + "";
						System.out.print("");
					}else if(polinomyalList2.get(i).getCoef()!=0){
						result = result + "+";
					System.out.print("+");
					}
				}
				if (polinomyalList2.get(i).getCoef()!=0){
				result = result + polinomyalList2.get(i).toString(); 
				System.out.print(polinomyalList2.get(i).toString());
				}
			}
			result=result.replace("x^0", "");
			return result;
		}
}