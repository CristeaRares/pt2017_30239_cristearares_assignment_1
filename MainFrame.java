
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainFrame {
	private JFrame mainFrame;
	private JPanel controlPanel;
	private JLabel polLabel1;
	private JLabel polLabel2;
	private JLabel resLabel;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;

	void prepareGUI() {
		mainFrame = new JFrame("Operatii Polinoame");
		mainFrame.setSize(600, 600);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

		controlPanel = new JPanel();
		controlPanel.setBackground(Color.LIGHT_GRAY);
		polLabel1 = new JLabel("Polinom 1: ", JLabel.RIGHT);
		textField1 = new JTextField(30);
		polLabel2 = new JLabel("Polinom 2: ", JLabel.RIGHT);
		textField2 = new JTextField(30);
		resLabel = new JLabel("Rezultat : ", JLabel.RIGHT);
		textField3 = new JTextField(60);
		JComboBox operatii1 = new JComboBox(Operatii.values());
		operatii1.addActionListener(new ActionListener() {

			
			public void actionPerformed(ActionEvent e) {
				Polinom p = new Polinom();
				List<Monom> polinomyalList = new ArrayList();
				List<Monom> polinomyalList1 = new ArrayList();

				JComboBox cb = (JComboBox) e.getSource();
				String pol1 = textField1.getText();
				String[] pol1Values = pol1.split(",");
				String pol2 = textField2.getText();
				String[] pol2Values = pol2.split(",");
				Operatii selName = (Operatii) cb.getSelectedItem();
				for (int i = 0; i < pol1Values.length; i += 2) {
					polinomyalList.add(new Monom(Double.parseDouble(pol1Values[i]), Integer.parseInt(pol1Values[i + 1])));
				}
				p.polinomyalList = polinomyalList;
				for (int i = 0; i < pol2Values.length; i += 2) {
					polinomyalList1.add(new Monom(Double.parseDouble(pol2Values[i]), Integer.parseInt(pol2Values[i + 1])));
				}
				p.polinomyalList1 = polinomyalList1;
				switch (selName) {
				case ADUNARE:
					p.addPolinom();
					textField3.setText(p.displayAdunare());
					break;
				case SCADERE:
					p.scaderePolinom();
					textField3.setText(p.displayScadere());
					break;
				case INMULTIRE:
					p.inmultirePolinom();
					textField3.setText(p.displayInmultire());
					break;
				case IMPARTIRE:
					p.impartirePolinom();
					textField3.setText(p.displayImpartire()+" rest "+p.displayImpartireRest());
					//textField3.setText(p.displayImpartireRest());
					break;
				case DERIVARE:
					p.derivarePolinom();
					textField3.setText(p.displayDerivare());
					break;
				case INTEGRARE:
					p.integrarePolinom();
					textField3.setText(p.displayIntegrare());
					break;

				default:
					break;
				}
			}
		});

		controlPanel.setLayout(new GridLayout(4, 2));
		controlPanel.add(polLabel1);
		controlPanel.add(textField1);
		controlPanel.add(polLabel2);
		controlPanel.add(textField2);
		controlPanel.add(resLabel);
		controlPanel.add(textField3);
		controlPanel.add(operatii1);
		controlPanel.setVisible(true);

		mainFrame.add(controlPanel);
		mainFrame.setVisible(true);
	}

	public static void main(String[] args) {
		MainFrame frame = new MainFrame();
		frame.prepareGUI();
	}
}
